#include "motiondetect.h"
#include <opencv2\opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

Rect get_motion_rect(Mat *frame,int max_dev, int min_changes_count) {
	Scalar mean, stddev;
	meanStdDev(*frame, mean, stddev);
	if (stddev[0] < max_dev) {
		int number_of_changes = 0;
		int min_x = frame->cols, max_x = 0;
		int min_y = frame->rows, max_y = 0;
		for(int j = 0; j < frame->rows; j+=2){ 
			for(int i = 0; i < frame->cols; i+=2){ 
				if(frame->at<uchar>(j,i) == 255) {
					number_of_changes++;
					if(min_x>i) min_x = i;
					if(max_x<i) max_x = i;
					if(min_y>j) min_y = j;
					if(max_y<j) max_y = j;
				}
			}
		}
		if (number_of_changes < min_changes_count)
			return Rect(0,0,0,0);
		if(min_y-10 > 0) min_y -= 10;
		if(max_x+10 < frame->cols-1) max_x += 10;
		if(max_y+10 < frame->rows-1) max_y += 10;
		Point x(min_x,min_y);
		Point y(max_x,max_y);
		return Rect(x,y);	
	}
	Point x(0,0);
	Point y(0,0);
	return Rect(x,y);
}

void diff_frames(Mat *frame_history,Mat &frame_rgb) {
	Mat dif1,dif2;
	//Mat frame[3];
	Mat frame_mask;
	const Scalar selection_color = Scalar(0,255,255);
	const int motion_changes_count = 5;
	const int max_deviation = 40;
	try {
		/*cvtColor(frame_history[2],frame[2],CV_RGB2GRAY);
		cvtColor(frame_history[1],frame[1],CV_RGB2GRAY);
		cvtColor(frame_history[0],frame[0],CV_RGB2GRAY);	
		*/absdiff(frame_history[2],frame_history[0],dif1);
		absdiff(frame_history[1],frame_history[0],dif2);
		bitwise_and(dif1,dif2,frame_mask);
		threshold(frame_mask,frame_mask,50,255,CV_THRESH_BINARY);
		Mat kernel_ero = getStructuringElement(MORPH_ELLIPSE, Size(2,2));
		erode(frame_mask,frame_mask,kernel_ero);
		Rect selection_rect = get_motion_rect(&frame_mask,max_deviation,motion_changes_count);
		rectangle(frame_rgb,selection_rect,selection_color);
	}
	catch(Exception error) {
		cout << error.what() << endl;
	}

}

vector<vector<Point>> contours;
Mat fore;
Mat back;
BackgroundSubtractorMOG2 bg(150,9);


void back_subtraction(Mat &frame)
{	
	bg(frame,fore);
	bg.getBackgroundImage(back);
	erode(fore,fore,Mat());
	dilate(fore,fore,Mat());
	findContours(fore,contours,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_NONE);
	drawContours(frame,contours,-1,Scalar(200,13,255),2);
}