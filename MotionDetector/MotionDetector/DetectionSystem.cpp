#include "DetectionSystem.h"
#include "facedetect.h"
#include <opencv2\highgui\highgui.hpp>
#include "motiondetect.h"
#include <ctime>
#include <Windows.h>
#include<opencv2/opencv.hpp>
#include<string>
using namespace cv;
using namespace std;

const string webcam_window_name = "detection system";
VideoCapture cap = VideoCapture(0);

Mat frame_rgb;
Mat frame_history[3];
Mat row_frame;
Mat images[8];


enum state
{
	main_window,
	face_detector,
	motion_detector,
	back_subtracting,
	exit_state,
	logo_state
};

int work_state = logo_state;

void onMouse(int event, int x, int y, int f, void*) 
{
	static int mouse_x = 0;
	static int mouse_y = 0;
	if (f & CV_EVENT_FLAG_LBUTTON) 
	{
		printf("mouse pos:%d %d\n",x,y);
		switch(work_state)
		{
		case main_window:
			if (x > frame_rgb.cols/2-75 && x < frame_rgb.cols/2+75 )
			{
				if (y > frame_rgb.rows/5 && y < frame_rgb.rows/5 + 50)
					work_state = motion_detector;
				if (y > frame_rgb.rows/5*2 && y < frame_rgb.rows/5*2 + 50)
					work_state = face_detector;
				if (y > frame_rgb.rows/5*3 && y < frame_rgb.rows/5*3 + 50)
					work_state = back_subtracting;
				if (y > frame_rgb.rows/5*4 && y < frame_rgb.rows/5*4 + 50)
					work_state = exit_state;
			}
			break;
		case face_detector:
		case motion_detector:
		case back_subtracting:
			if (x > frame_rgb.cols - 180 && x < frame_rgb.cols - 30 )
				if (y > frame_rgb.rows - 60 && y < frame_rgb.rows - 10)
					work_state = main_window;
			if (x > 30 && x < 180 )
				if (y > frame_rgb.rows - 60 && y < frame_rgb.rows - 10)
				{
					time_t rawtime;
					struct tm * timeinfo;
					time (&rawtime);
					timeinfo = localtime(&rawtime);
					char *buffer = new char[20];
					strftime(buffer,20,"%H%M%S",timeinfo);
					imwrite(string("images\\img")+string(buffer)+string(".jpg"),row_frame);

				}
				break;
		case logo_state:
			work_state = main_window;
		default :
			break;
		}
	}
}


int load_images()
{
	images[0] = imread("logo.jpg");
	images[1] = imread("buttons//open_diff_frames.jpg");
	images[2] = imread("buttons//open_fd.jpg");
	images[3] = imread("buttons//open_exit.jpg");
	images[4] = imread("buttons//open_back_est.jpg");
	images[5] = imread("buttons//back.jpg");
	images[6] = imread("buttons//screenshot.jpg");
	for (int i=0; i<7; i++)
		if (images[i].data == NULL)
		{
			MessageBox(0,L"Can't load images",L"Error",0);
			return 0;
		}
	return 1;
}

int init_window() 
{
	try
	{
		namedWindow(webcam_window_name);
		moveWindow(webcam_window_name,300,100);
		setMouseCallback(webcam_window_name,onMouse,0);
		for(int i = 0; i< 3; i++)
			frame_history[i] = images[0];
	}
	catch(...)
	{
		MessageBox(0,L"Can't init window!",L"Error",0);
		return 0;
	}
	return 1;
}

int init_webcam() 
{
	try
	{
		frame_rgb = images[0];
		imshow(webcam_window_name,frame_rgb);
	}
	catch(...)
	{
		MessageBox(0,L"Can't init webcam",L"Error",0);
		return 0;
	}
	while (work_state == logo_state )
		waitKey(100);
	if(!cap.isOpened())  
		return 0;
	return 1;
}

int get_frame() 
{
	cap >> frame_rgb;
	Mat gray;
	if (!frame_rgb.data)
	{
		MessageBox(0,L"Can't grab image",L"Error",0);
		work_state = exit_state;
		return 0;
	}
	for(int i = 2; i > 0; i--)
		frame_history[i-1].copyTo(frame_history[i]);
	cvtColor(frame_rgb,frame_history[0],CV_RGB2GRAY);
	return 1;
}

void show_frame() 
{
	imshow(webcam_window_name,frame_rgb);  
}

int check_key() {
	char key = waitKey(10);
	if (key == 27)
		return 0;
	return 1;
}

void draw_button(int num,Point pos)
{
	Mat back = images[num];
	Rect roi(pos,Size(150,50));
	back.copyTo(frame_rgb(roi));
}

void show_buttons()
{
	switch (work_state)
	{
	case main_window:
		draw_button(1,Point(frame_rgb.cols/2-75,frame_rgb.rows/5*1));
		draw_button(2,Point(frame_rgb.cols/2-75,frame_rgb.rows/5*2));
		draw_button(3,Point(frame_rgb.cols/2-75,frame_rgb.rows/5*4));
		draw_button(4,Point(frame_rgb.cols/2-75,frame_rgb.rows/5*3));
		break;
	case motion_detector:
	case back_subtracting:
	case face_detector:
		draw_button(5,Point(frame_rgb.cols-180,frame_rgb.rows-60));
		draw_button(6,Point(30,frame_rgb.rows-60));
		break;
	default:
		break;
	}
}

void show_main_window()
{
	if (!get_frame())
		return;
	show_buttons();
	show_frame();

}

void show_motion_detector()
{
	if (!get_frame())
		return;
	diff_frames(frame_history,frame_rgb);
	frame_rgb.copyTo(row_frame);
	show_buttons();
	show_frame();
	waitKey(50);
}

void show_md_back()
{
	if (!get_frame())
		return;
	back_subtraction(frame_rgb);
	frame_rgb.copyTo(row_frame);
	show_buttons();
	show_frame();
	waitKey(10);
}

void show_face_detector()
{
	if (!get_frame())
		return;
	if (!detectFace(frame_rgb))
	{
		work_state = main_window;
	}
	frame_rgb.copyTo(row_frame);
	show_buttons();
	show_frame();
	waitKey(10);
}

void run_md()
{
	if (!load_images())
		return;
	if (!init_window())
		return;
	if (!init_webcam())
		return;
	if (!faceDetectorInit())
		return;
	int is_run = 1;
	work_state = main_window;
	while(is_run)
	{
		is_run = check_key();
		switch (work_state)
		{
		case main_window:
			show_main_window();
			break;
		case motion_detector:
			show_motion_detector();
			break;
		case back_subtracting:
			show_md_back();
			break;
		case face_detector:
			show_face_detector();
			break;
		default:
			is_run = false;
			break;
		}
	}
}