#pragma once
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace cv;

void diff_frames(Mat *frame_history,Mat &frame_rgb);
void back_subtraction(Mat &frame);