#include "facedetect.h"


//#include <opencv2\gpu\gpumat.hpp>
using namespace std;
using namespace cv;
using namespace cv::gpu;

//#include <opencv2\gpu\gpu.hpp>

#include <Windows.h>


string cascadeName = "cascades\\haarcascade_frontalface_alt.xml";
string nestedCascadeName = "cascades\\haarcascade_eye_tree_eyeglasses.xml";
const Scalar colors[] =  { CV_RGB(0,0,255),
	CV_RGB(0,128,255),
	CV_RGB(0,255,255),
	CV_RGB(0,255,0),
	CV_RGB(255,128,0),
	CV_RGB(255,255,0),
	CV_RGB(255,0,0),
	CV_RGB(255,0,255)} ;

vector<Rect> faces, faces2;

CascadeClassifier cascade;
CascadeClassifier nestedCascade;
//CascadeClassifier_GPU cascade_gpu;
//CascadeClassifier_GPU nestedCascade_gpu;


int faceDetectorInit()
{
	cascade.load(cascadeName);
	nestedCascade.load(nestedCascadeName);
	//cascade_gpu.load(cascadeName);
	//nestedCascade_gpu.load(nestedCascadeName);
	if (cascade.empty() || nestedCascade.empty())
	{
		MessageBox(0,L"Cascades not find",L"Error",0);
		return 0;
	}
	return 1;
}

int detectFace( Mat& img)
{
	int i = 0;
	double t = 0;
	Mat gray;
	cvtColor( img, gray, CV_BGR2GRAY );
	cascade.detectMultiScale( gray, faces );
	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Mat ImgROI;
		vector<Rect> nestedObjects;
		Point center;
		Scalar color = colors[i%8];
		int radius;
		rectangle( img, cvPoint(r->x, r->y),
			cvPoint(r->x + r->width-1, (r->y + r->height-1)),
			color, 3, 8, 0);
		ImgROI = gray(*r);
		nestedCascade.detectMultiScale( ImgROI, nestedObjects );
		for( vector<Rect>::const_iterator nr = nestedObjects.begin(); nr != nestedObjects.end(); nr++ )
		{
			center.x = cvRound(r->x + nr->x + nr->width*0.5);
			center.y = cvRound(r->y + nr->y + nr->height*0.5);
			radius = cvRound((nr->width + nr->height)*0.25);
			circle( img, center, radius, color, 3, 8, 0 );
		}
	}
	return 1;
}

//int detectFaceGPU(Mat& img)
//{
//	gpu::GpuMat faceBuf_gpu;  
//	gpu::GpuMat GpuImg;  
//	gpu::GpuMat eyesBuf_gpu;  
//	Mat grayImg;
//	cvtColor( img, grayImg, CV_BGR2GRAY );
//	GpuImg.upload(grayImg);  
//	int detectionNumber = cascade_gpu.detectMultiScale(GpuImg, faceBuf_gpu);  
//	Mat faces_downloaded, eyes_downloaded;  
//	if(detectionNumber > 0)  
//	{  
//		Point center;
//		int radius;
//		faceBuf_gpu.colRange(0, detectionNumber).download(faces_downloaded);  
//		Rect* faces = faces_downloaded.ptr<Rect>();  
//		for(int i = 0; i < detectionNumber; i++)  
//		{  
//			Scalar color = colors[i%8];
//			Mat faceROI = grayImg(faces[i]);
//			GpuImg.upload(faceROI);
//			int eyesCount = nestedCascade_gpu.detectMultiScale(GpuImg, eyesBuf_gpu); 
//			if(eyesCount > 0)  
//			{
//				eyesBuf_gpu.colRange(0, eyesCount).download(eyes_downloaded);  
//				Rect* eyes = eyes_downloaded.ptr<Rect>(); 
//				for(int j = 0; j < eyesCount; j++)  
//				{  
//					center.x = cvRound(faces[i].x + eyes[j].x + eyes[j].width*0.5);
//					center.y = cvRound(faces[i].y + eyes[j].y + eyes[j].height*0.5);
//					radius = cvRound((eyes[j].width + eyes[j].height)*0.25);
//					circle( img, center, radius, color, 3);
//				}
//			}
//			rectangle(img, Point(faces[i].x, faces[i].y), Point(faces[i].x+faces[i].width, faces[i].y+faces[i].height), color, 3);  
//		}  
//	}  
//	return 1;
//}